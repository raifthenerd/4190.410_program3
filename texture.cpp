#include <fstream>
#include <cmath>
#include "texture.h"
#include "lodepng.h"

unsigned char *loadTGA(const char* filepath, int &width, int &height) {

    std::ifstream fin(filepath, std::ios::binary);
    if (fin)
    {
        printf("[Texture Loading] : %s\n", filepath);
        unsigned char header[12];
        unsigned char info[6];
        // read header/info from file
        fin.read((char*)header, 12);
        fin.read((char*)info, 6);
        // translate header/info
        width = info[0] | (info[1] << 8);
        height = info[2] | (info[3] << 8);
        int depth =   info[4] / 8;
        printf("width: %d, height: %d\n", width, height);

        // read data
        unsigned char *data = new unsigned char[width*height*4];
        unsigned char *temp = new unsigned char[width*height*depth];
        fin.read((char*)temp, width*height*depth);
        for(int j=0; j<height; ++j)
        for(int i=0; i<width; ++i) {
            data[4*(j*width+i)  ] = temp[depth*(j*width+i)+2];
            data[4*(j*width+i)+1] = temp[depth*(j*width+i)+1];
            data[4*(j*width+i)+2] = temp[depth*(j*width+i)  ];
            data[4*(j*width+i)+3] = depth==4 ? temp[depth*(j*width+i)+3] : 255;
        }
        delete [] temp; fin.close();

        return data;
    }
    else return NULL;
}

void initTGA(GLuint *tex, const char *name, int &width, int &height) {  
    unsigned char *data = loadTGA(name, width, height); 
    glDeleteTextures(1, tex);
    glGenTextures(1, tex);
    glBindTexture(GL_TEXTURE_2D, *tex);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);   
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data); 
    if(data) delete [] data;
}

void initPNG(GLuint *tex, const char *sp_name, const char *cb_name) {
    LodePNG_Decoder decoder;
    LodePNG_Encoder encoder;
    unsigned char* buffer;
    unsigned char* img_sp;
    unsigned char* img_cb;
    size_t buffersize, imgsize_sp, imgsize_cb;
    int width_sp, height_sp, width_cb, height_cb;

    LodePNG_Decoder_init(&decoder);
    LodePNG_loadFile(&buffer, &buffersize, sp_name); /*load the image file with given filename*/
    if ( !buffer || buffersize <= 0 ){
        printf("Couldn't open file\n");
        return;
    }
    LodePNG_Decoder_decode(&decoder, &img_sp, &imgsize_sp, buffer, buffersize); /*decode the png*/
    width_sp = decoder.infoPng.width;
    height_sp = decoder.infoPng.height;
    LodePNG_loadFile(&buffer, &buffersize, cb_name); /*load the image file with given filename*/
    if ( !buffer || buffersize <= 0 ){
        printf("Couldn't open file\n");
        return;
    }
    LodePNG_Decoder_decode(&decoder, &img_cb, &imgsize_cb, buffer, buffersize); /*decode the png*/
    width_cb = decoder.infoPng.width;
    height_cb = decoder.infoPng.height;

    // convert single sphere map into dual parabolic map
    unsigned char* para_sp = new unsigned char[4*1000*500];
    unsigned char* para_cb = new unsigned char[4*1000*500];
    for (int i=0; i<500; ++i) {
        for (int j=0; j<500; ++j) {
            float x = (float) i / (500 - 1);
            float y = (float) j / (500 - 1);
            x = 2*x-1; y = 2*y-1;
            float z = (1-x*x-y*y)/2;
            float m = sqrt(x*x+y*y+z*z);
            x = x/m; y = y/m; z = z/m; // (x,y,z) in this line is recovered r
            float newx, newy, newz;
            int newi, newj;
            newz = z+1;
            m = sqrt(x*x+y*y+newz*newz);
            newx = x/m; newy=y/m;
            newx = 0.5*newx+.5; newy = 0.5*newy+0.5;
            newi = (int)((width_sp-1)*newx)%width_sp;
            newj = (int)((height_sp-1)*newy)%height_sp;
            for (int k=0; k<4; ++k) {
                para_sp[4*(500+i+1000*j)+k] = img_sp[4*(newi+width_sp*newj)+k];
            }
            newz = -z+1;
            m = sqrt(x*x+y*y+newz*newz);
            newx = x/m; newy=y/m;
            newx = 0.5*newx+.5; newy = 0.5*newy+0.5;
            newi = (int)((width_sp-1)*newx)%width_sp;
            newj = (int)((height_sp-1)*newy)%height_sp;
            for (int k=0; k<4; ++k) {
                para_sp[4*(i+1000*j)+k] = img_sp[4*(newi+width_sp*newj)+k];
            }
            float d;
            newx = x > 0 ? x : -x; newy = y > 0 ? y : -y; newz = z;
            if (newx >= newy && newx >= newz) {
                if (x > 0) {
                    newx = 2.5-0.5*y/x;
                    newy = 1.5-0.5*z/x;
                    d = z/x;
                } else {
                    newx = 0.5-0.5*y/x;
                    newy = 1.5+0.5*z/x;
                    d = -z/x;
                }
            } else if (newy >= newx && newy >= newz) {
                if (y > 0) {
                    newx = 1.5+0.5*x/y;
                    newy = 1.5-0.5*z/y;
                    d = z/y;
                } else {
                    newx = 3.5+0.5*x/y;
                    newy = 1.5+0.5*z/y;
                    d = -z/y;
                }
            } else if (newz >= newx && newz >= newy) {
                newx = 1.5+0.5*x/z;
                newy = 0.5+0.5*y/z;
                d = 2-y/z;
            }
            newx /= 4.0; newy /= 3.0;
            newi = (int)((width_cb-1)*newx)%width_cb;
            newj = (int)((height_cb-1)*newy)%height_cb;
            for (int k=0; k<4; ++k) {
                para_cb[4*(500+i+1000*j)+k] = img_cb[4*(newi+width_cb*newj)+k];
            }
            newy += d/3.0;
            newi = (int)((width_cb-1)*newx)%width_cb;
            newj = (int)((height_cb-1)*newy)%height_cb;
            for (int k=0; k<4; ++k) {
                para_cb[4*(i+1000*j)+k] = img_cb[4*(newi+width_cb*newj)+k];
            }
        }
    }

    glGenTextures(2, tex);

    glBindTexture(GL_TEXTURE_2D, tex[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1000, 500, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, para_sp);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glBindTexture(GL_TEXTURE_2D, tex[1]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1000, 500, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, para_cb);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    LodePNG_Encoder_init(&encoder);
    LodePNG_Encoder_encode(&encoder, &buffer, &buffersize, para_sp, 1000, 500);
    LodePNG_saveFile(buffer, buffersize, "parabolic_sphere.png");
    LodePNG_Encoder_encode(&encoder, &buffer, &buffersize, para_cb, 1000, 500);
    LodePNG_saveFile(buffer, buffersize, "parabolic_cube.png");
    free(buffer);
    free(img_sp);
    free(img_cb);
    free(para_sp);
    free(para_cb);
}

void initTex() {

}
