#ifndef JH_TEXTURE_H
#define JH_TEXTURE_H

#include <GL/freeglut.h>

unsigned char *loadTGA(const char* filepath, int &width, int &height);
void initTGA(GLuint *tex, const char *name, int &width, int &height);
void initPNG(GLuint *tex, const char *sp_name, const char *cb_name);
void initTex();

#endif
